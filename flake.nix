{
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        # Fetch the substack archives starting from the given offset
        archivesWithOffset = offset: builtins.fromJSON (builtins.readFile (builtins.fetchurl "https://draculadaily.substack.com/api/v1/archive?offset=${builtins.toString offset}"));

        # The substack archives before Jan 1st 2022
        archives =
          let
            archivesAfter = ar: (ar ++ archivesWithOffset (builtins.length ar));
            maxDate = "2022-01-01T00:00:00Z";
          in
          builtins.filter (a: a.post_date < maxDate) (pkgs.lib.converge archivesAfter [ ]);

        # A single page, corresponding to a given day
        page = entry:
          let
            url = entry.canonical_url;
            date = entry.post_date;
            file = builtins.fetchurl url;
          in
          pkgs.runCommand "dracula-daily-${date}.html" { buildInputs = [ pkgs.htmlq ]; } ''
            cat ${file} | htmlq --pretty 'div.available-content > div' > $out
          '';

        # All the pages together in a folder
        pages = pkgs.linkFarmFromDrvs "dracula-daily" (map page archives);

        # The actual ebook
        book = pkgs.runCommand "dracula-daily.epub" { buildInputs = [ pkgs.pandoc ]; } ''
          cd ${pages}
          ls ${pages} | xargs pandoc -f html -t epub --metadata=title:"Dracula Daily" -o $out
        '';
      in
      {
        packages = {
          default = book;
        };

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            curl
            jq
            fx
            yq
            htmlq
          ];
        };
      }
    );
}
