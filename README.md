# Dracula Daily EPUB

Maybe you wanna do [Dracula Daily](https://draculadaily.substack.com/) but you don't like reading on your phone or on your computer.  You can't simply get a free Dracula EPUB, because Dracula Daily tells the story in a different order from the original book.

I'm here to relieve you from this plight.  This repo contains [an EPUB of the original 2021 run of Dracula Daily](https://codeberg.org/xlambein/dracula-daily-epub/raw/branch/main/dracula-daily.epub), which you can enjoy from the comfort of your ebook reader.

You can also build it yourself with Nix Flakes:

```bash
nix build --impure --no-link --print-out-paths | xargs -I {} cp --no-preserve=mode {} -T dracula-daily.epub
```

This will put a copy of `dracula-daily.epub` in the current directory.  You might have to run it multiple times until it succeeds because you'll probably get HTTP 429 ("too many requests") errors.
